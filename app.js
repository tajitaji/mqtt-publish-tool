var basicAuth = require("basic-auth-connect");
var mqtt = require("mqtt");
var bodyParser = require("body-parser");
var express = require("express");
var app = express();

var host = "160.16.96.11";
var port = 8883;
var uid = "aptj";
var pass = "aptj-mqtt";

app.set("views", "./views");
app.set("view engine", "ejs");

app.use(basicAuth(uid, pass));
app.use(bodyParser.urlencoded({extended: true}));
app.use("/public", express.static("public"));

app.get("/", function(req, res) {
  res.render("pub", {});
});

app.post("/pub", function(req, res) {
  if (req.body && req.body.topic && req.body.topicval && req.body.count && req.body.interval) {
    var delay = parseInt(req.body.delay);
    var count = parseInt(req.body.count);
    var interval = parseInt(req.body.interval);
    var qos = parseInt(req.body.qos) || 0;
    var url = "mqtt://160.16.96.11";
    var opt = {
      port: 8883,
      clientId: "aptj_" + Math.random().toString(16).substr(2, 8),
      username: uid,
      password: pass
    };
    var client = mqtt.connect(url, opt);
    client.on("connect", function() {
      var dopub = function() {
        var timeoutId = setTimeout(function() {
          if (intervalId) {
            clearInterval(intervalId);
            if (client) client.end();
          }
        }, count * interval + 1000);
        var intervalId = setInterval(function() {
          client.publish(req.body.topic, req.body.topicval, {qos:qos});
          count--;
          if (count <= 0) {
            clearInterval(intervalId);
            clearTimeout(timeoutId);
            client.end();
            res.writeHead(200, {"Content-type": "text/html"});
            res.write("finished");
            res.end();
          }
        }, interval);
      };
      if (delay) {
        setTimeout(dopub, delay);
      } else {
        dopub();
      }
    });
  }
});

var server = app.listen(8884, function() {
  console.log("Node server is listening to PORT:" + server.address().port);
});
